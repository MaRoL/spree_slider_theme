module SpreeSliderTheme
  module Generators
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path('../templates', __FILE__)

      def add_javascripts
        append_file 'app/assets/javascripts/store/all.js', "//= require store/spree_slider_theme\n"
        append_file 'app/assets/javascripts/admin/all.js', "//= require admin/spree_slider_theme\n"
      end

      def add_stylesheets
        inject_into_file 'app/assets/stylesheets/store/all.css', " *= require store/spree_slider_theme\n", :before => /\*\//, :verbose => true
        inject_into_file 'app/assets/stylesheets/store/all.css', " *= require store/spree_slider_theme/default\n", :before => /\*\//, :verbose => true
      end

      def copy_locales
        template 'config/locales/spree_slider_theme/id.yml'
        template 'config/locales/spree_slider_theme/en.yml'
      end

      def copy_no_image
        template "public/spree_slider_theme/no_image.jpg"
      end

      def add_migrations
        run 'bundle exec rake railties:install:migrations FROM=spree_slider_theme'
      end

      def run_migrations
         res = ask 'Would you like to run the migrations now? [Y/n]'
         if res == '' || res.downcase == 'y'
           run 'bundle exec rake db:migrate'
         else
           puts 'Skipping rake db:migrate, don\'t forget to run it!'
         end
      end

      def add_seeds
        SpreeSliderTheme::Engine.load_seed
        puts "Seeds for SpreeSliderTheme added"
      end
    end
  end
end
