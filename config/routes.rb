Spree::Core::Engine.routes.draw do
  namespace :spree do
    namespace :spree_slider_theme do
      resources :theme_colors
      resources :infos
    end
  end

  scope :module => 'spree_slider_theme' do
    resources :infos
    match "order_information" => "infos#order_information", :via => :get, :as => "order_information"
    match "search_order" => "infos#search_order", :via => :post, :as => "search_order"
  end

  namespace :admin do
    namespace :spree_slider_theme do
      resources :theme_images
      resources :infos
    end
  end

  namespace :spree_slider_theme do
    resources :theme_images
  end

  namespace :admin do
    namespace :spree do
      namespace :spree_slider_theme do
       resources :theme_colors
      end
    end
  end

  namespace :admin, :scope => "spree_slider_theme/theme_images" do
    match "enabled_selected" => "spree_slider_theme/theme_images#enabled_selected", :via => :post, :as => "enabled_selected"
    match "redirect_enabled_selected" => "spree_slider_theme/theme_images#redirect_enabled_selected", :via => :get, :as => "redirect_enabled"
  end

  namespace :admin, :scope => "spree_slider_theme/theme_colors" do
    match "edit_theme" => "spree_slider_theme/theme_colors#edit_theme", :via => :get, :as => "edit_theme"
    match "update_theme" => "spree_slider_theme/theme_colors#update_theme", :via => :post, :as => "update_theme"
  end

  # Route for newest_product, best_product
  get '/best_seller/' => 'products#best_product'
  get '/new/' => 'products#newest_product'
  get '/promo/' => 'products#promotion_product'

  match "locale/change" => "locale#change_locale", :as => "change_locale"
end
