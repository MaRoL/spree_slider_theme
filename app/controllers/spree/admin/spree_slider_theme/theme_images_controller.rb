module Spree
  module Admin
    class SpreeSliderTheme::ThemeImagesController < Spree::Admin::BaseController
      def index
        @images = Spree::SpreeSliderTheme::ThemeImage.all
        @image = Spree::SpreeSliderTheme::ThemeImage.new
        @theme_color = Spree::SpreeSliderTheme::ThemeColor.where(:used => "true").first
        @theme_colors = Spree::SpreeSliderTheme::ThemeColor.find(:all, :order => "used DESC")
      end

      def new
        @image = Spree::SpreeSliderTheme::ThemeImage.new
      end

      def create
        @images = params[:spree_slider_theme_theme_image][:image]
        if @images.count > 1 # NOTE for multiple upload
          logger.debug("----------------------------------multipe upload")
          @index = 0
          @images.each do |image|
            @image = Spree::SpreeSliderTheme::ThemeImage.create(:image => params[:spree_slider_theme_theme_image][:image][@index])
            if @image.new_record?
              @error = true
            else
              @error = false
            end
            @index += 1
          end

          if @error == true
            redirect_to spree.admin_spree_slider_theme_theme_images_path, :notice => @image.errors.messages[:file].first
          else
            redirect_to spree.admin_spree_slider_theme_theme_images_path, :notice => (I18n.t "messages.theme_images.succeed_add")
          end
        else
          @image = Spree::SpreeSliderTheme::ThemeImage.create(:image => params[:spree_slider_theme_theme_image][:image][0])
          if @image.new_record?
            redirect_to spree.admin_spree_slider_theme_theme_images_path, :notice => @image.errors.messages[:file].first
          else
            redirect_to spree.admin_spree_slider_theme_theme_images_path, :notice => (I18n.t "messages.theme_images.succeed_add")
          end
        end
      end

      def edit
        @image = Spree::SpreeSliderTheme::ThemeImage.find(params[:id])
      end

      def update
        @image = Spree::SpreeSliderTheme::ThemeImage.find(params[:id])
        if @image.update_attributes(params[:spree_slider_theme_theme_image])
          redirect_to spree.admin_spree_slider_theme_theme_images_path, notice: (I18n.t "messages.theme_images.succeed_edit")
        else
          render action: "edit", :error => @image.errors
        end
      end

      def destroy
        @image = Spree::SpreeSliderTheme::ThemeImage.find(params[:id])
        if @image.destroy
          redirect_to spree.admin_spree_slider_theme_theme_images_path, :notice => (I18n.t "messages.theme_images.succeed_destroy")
        else
          redirect_to spree.admin_spree_slider_theme_theme_images_path, :notice => @image.errors
        end
      end

      def enabled_selected
        @arr_images = params[:arr_images]
        @arr_images_o = Spree::SpreeSliderTheme::ThemeImage.where(:enabled => "true")
        @arr_images_o.each do |image|
          image.enabled = "false"
          image.save
        end

        @arr_images.each do |image|
          @data = Spree::SpreeSliderTheme::ThemeImage.find(image)
          @data.enabled = "true"

          if @data.save
            @notice = "s"
          else
            @notice = "f"
          end
        end

        respond_to do |format|
          format.json { render :json => {:notice => @notice} }
        end
      end

      def redirect_enabled_selected
        if params[:notice] == "s"
          @notice = I18n.t "messages.succeed"
        else
          @notice = I18n.t "messages.failed"
        end

        redirect_to spree.admin_spree_slider_theme_theme_images_path, :notice => @notice
      end
    end
  end
end
