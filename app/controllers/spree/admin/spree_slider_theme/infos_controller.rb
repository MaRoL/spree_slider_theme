class Spree::Admin::SpreeSliderTheme::InfosController < Spree::Admin::BaseController
  def index
    @infos = Spree::SpreeSliderTheme::Info.all
  end

  def edit
    @info = Spree::SpreeSliderTheme::Info.find(params[:id])
  end

  def update
    @info = Spree::SpreeSliderTheme::Info.find(params[:id])
    if @info.update_attributes(params[:spree_slider_theme_info])
      redirect_to admin_spree_slider_theme_infos_path, :notice => "#{t("fat_footer.#{@info.title}")} #{t(:updated)}"
    else
      render action: "edit", :error => @info.errors
    end
  end
end
