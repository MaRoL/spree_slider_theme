class Spree::SpreeSliderTheme::InfosController < Spree::BaseController
  helper "Spree::Products"
  include Spree::SpreeSliderTheme::InfosHelper
  before_filter :new_variable

  def new_variable
    load_new_variable
  end

  def show
    @info = Spree::SpreeSliderTheme::Info.find(params[:id])
  end

  def order_information
    @order = Spree::Order.new
  end

  def search_order
    @order = Spree::Order.find_by_number(params[:order][:number])
    @customer = "#{@order.bill_address.firstname} #{@order.bill_address.lastname}" unless @order.nil?
    respond_to do |format|
      format.js
    end
  end
end
