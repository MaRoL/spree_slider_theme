Deface::Override.new(:virtual_path  => "spree/home/index",
                     :insert_after  => "[data-hook='homepage_products']",
                     :name          => "jquery_fat_footer_main_page",
                     :partial       => "spree/home/fat_footer_main_page")

Deface::Override.new(:virtual_path  => "spree/home/index",
                     :remove        => "[data-hook='homepage_products']",
                     :name          => "delete_product_list")

Deface::Override.new(:virtual_path  => "spree/layouts/_slider",
                     :insert_bottom => ".slider_pict",
                     :name          => "insert_3_pictures",
                     :partial       => "spree/layouts/home_pictures")
