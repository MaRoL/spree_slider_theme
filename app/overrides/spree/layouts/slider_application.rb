Deface::Override.new(:virtual_path  => "spree/layouts/spree_application",
                     :insert_bottom => "[data-hook='inside_head']",
                     :name          => "slider_include",
                     :text          => "<%= javascript_include_tag 'store/nivo-slider/jquery.nivo.slider.js' %>
                                        <br/><%= stylesheet_link_tag 'store/nivo-slider/themes/default/default.css' %>
                                        <br/><%= stylesheet_link_tag 'store/nivo-slider/nivo-slider.css' %>")

Deface::Override.new(:virtual_path => "spree/layouts/spree_application",
                     :insert_top   => "div#wrapper",
                     :name         => "slider",
                     :partial      => "spree/layouts/slider")
