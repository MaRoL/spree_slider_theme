Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "menu_slider_theme",
                     :insert_bottom => "[data-hook='admin_tabs'], #admin_tabs[data-hook]",
                     :partial => "spree/layouts/menu_theme")
