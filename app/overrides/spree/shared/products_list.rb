Deface::Override.new(:virtual_path => "spree/shared/_products",
                     :remove => "span.price",
                     :name => "remove_price_product")

Deface::Override.new(:virtual_path => "spree/shared/_products",
                     :insert_after => "div.product-image",
                     :name => "insert_new_product_info",
                     :partial => "spree/shared/new_product_info")
