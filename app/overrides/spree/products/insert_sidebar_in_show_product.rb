Deface::Override.new(:virtual_path => "spree/products/show",
                     :insert_before => "[data-hook='product_show']",
                     :name => "insert_sidebar_in_show_product",
                     :partial => "spree/products/product_detail_sidebar")

Deface::Override.new(:virtual_path => "spree/products/show",
                     :replace => "[data-hook='product_right_part']",
                     :name => "replace_product_right_part",
                     :partial => "spree/products/product_right_part")
