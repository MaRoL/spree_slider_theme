module Spree
  module SpreeSliderTheme
    class ThemeColor < ActiveRecord::Base
      self.table_name = 'spree_spree_slider_theme_theme_colors'
      attr_accessible :color, :filename, :used
    end
  end
end
