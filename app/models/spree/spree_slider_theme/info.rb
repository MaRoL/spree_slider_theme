class Spree::SpreeSliderTheme::Info < ActiveRecord::Base
  self.table_name = 'spree_spree_slider_theme_infos'
  attr_accessible :body, :title
end
