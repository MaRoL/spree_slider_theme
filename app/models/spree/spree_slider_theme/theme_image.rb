module Spree
  module SpreeSliderTheme
    class ThemeImage < ActiveRecord::Base
      self.table_name = 'spree_slider_theme_theme_images'
      attr_accessible :image, :enabled, :url_destination, :description

      has_attached_file :image, :styles => { :small => "200x200>", :thumb => "100x100>" }
      # validate :file_dimensions, :unless => "errors.any?", :on => :create

      def file_dimensions
        dimensions = Paperclip::Geometry.from_file(image.queued_for_write[:original].path)
        # width  : 1024px
        # height : 510px
        if dimensions.width < 974 || dimensions.width > 1700
          errors.add(:file, (I18n.t "messages.theme_images.range_width", :range_1 => "974", :range_2 => "1700"))
        end

        if dimensions.height < 460 || dimensions.height > 1100
          errors.add(:file, (I18n.t "messages.theme_images.range_height", :range_1 => "460", :range_2 => "510"))
        end
      end
    end
  end
end
