module Spree
  TaxonsController.class_eval do
    before_filter :show_from_detail, :only => :show

    def show_from_detail
      # logger.debug("-------------------------------show from detail")
      # logger.debug("-------------------------------params[:id] : #{params[:id]}")
      # logger.debug("-------------------------------find product : #{Product.find_by_permalink(params[:id])}")
      # logger.debug("-------------------------------nil? #{Product.find_by_permalink(params[:id]).nil?}")
      if Product.find_by_permalink(params[:id]).nil?
        params[:id] = params[:id]
      else
        # logger.debug("-----------------------------#{Product.find_by_permalink!(params[:id])}")
        params[:id] = Product.find_by_permalink(params[:id]).taxons.first.permalink
      end
    end
  end
end
