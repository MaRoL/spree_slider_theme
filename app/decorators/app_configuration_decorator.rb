include Spree::SpreeSliderTheme::InfosHelper
module Spree
  AppConfiguration.class_eval do
    attr_accessor :built_in_locales

    def built_in_locales
      @built_in_locales = {}
      @built_in_locales[:en] = "English" if check_locales(:en)
      @built_in_locales[:id] = "Indonesia" if check_locales(:id)
      @built_in_locales
    end
  end
end
