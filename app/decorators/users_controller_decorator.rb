module Spree
  UsersController.class_eval do
    helper "Spree::Products"
    include Spree::SpreeSliderTheme::InfosHelper
    before_filter :new_variable

    def new_variable
      load_new_variable
    end
  end
end
