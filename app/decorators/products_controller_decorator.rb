module Spree
  ProductsController.class_eval do
    helper 'spree/products'
    before_filter :append_taxon, :only => :show

    def append_taxon
      @taxon = Product.find_by_permalink(params[:id]).taxons.first
      if @taxon.nil?
        @taxon = Taxon.find_by_permalink(params[:id])
      end
      @searcher = Spree::Config.searcher_class.new(params.merge(:taxon => @taxon.id))
      @searcher.current_user = try_spree_current_user
      @products = @searcher.retrieve_products
    end

    def best_product
      arr = []
      Spree::Product.all.each do |product|
        variants = product.variants_including_master_and_deleted
        line_item = Spree::LineItem.find(:all, :conditions => ["variant_id IN (?)", variants ] )
        quantities = line_item.map(&:quantity)
        quantity_total = quantities.inject{|sum,x| sum + x}.nil? ? 0 : quantities.inject{|sum,x| sum + x}
        arr.push({:product_id => product.id, :quantity => quantity_total})
      end
      @products = arr.sort {|a, b| b[:quantity] <=> a[:quantity]}.slice!(0, 5)
      @products = Spree::Product.find(:all, :conditions => ["id IN (?)", @products.collect{ |p| p[:product_id] }], :limit => 9)
      if @products.nil?
        @products = Spree::Product.limit(9)
      end
    end

    def newest_product
      @products = Spree::Product.order("created_at DESC").limit(9)
      if @products.nil?
        @products = Spree::Product.limit(9)
      end
    end

    def promotion_product
      @products = []
      Spree::Promotion::Rules::Product.all.each do |promotion|
        promotion.products.each do |product|
          @products.push(product)
        end
      end
      if @products.empty?
        @products = Spree::Product.limit(9)
      end
    end

    private
      def accurate_title
        @taxon ? @taxon.name : super
      end
  end
end
