//= require store/spree_core
//= require jquery-ui

$(document).ready(function(){
  $('#slider').nivoSlider({effect: 'fade', pauseOnHover: false, randomStart: true, controlNav: false, galleries: true, animSpeed: 1000 });
});
