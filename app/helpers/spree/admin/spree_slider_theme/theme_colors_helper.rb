module Spree::Admin::SpreeSliderTheme::ThemeColorsHelper
  def change_color(old_theme, new_theme)
    @file = File.readlines('./app/assets/stylesheets/store/all.css')
    index = 0
    find = false

    unless old_theme.nil?
      @file.each do |f|
        if f.include?(old_theme.filename)
          find = true
          break
        end
        index += 1
      end
    end
    @file.delete_at(index) if find
    index = @file.count - 2 if index == 0
    @file.insert(index, " *= require store/spree_slider_theme/#{new_theme.filename}\n")
    File.open('./app/assets/stylesheets/store/all.css', 'w+') { |f| f.write @file.join() }
  end
end
