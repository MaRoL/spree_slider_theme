class CreateSpreeSpreeSliderThemeInfos < ActiveRecord::Migration
  def change
    create_table :spree_spree_slider_theme_infos do |t|
      t.string :title
      t.string :body

      t.timestamps
    end
  end
end
