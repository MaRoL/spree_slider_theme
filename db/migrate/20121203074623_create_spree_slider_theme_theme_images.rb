class CreateSpreeSliderThemeThemeImages < ActiveRecord::Migration
  def change
    create_table :spree_slider_theme_theme_images do |t|
      t.string  :enabled, :default => "false"
      t.has_attached_file :image
      t.string :description
      t.string :url_destination, :default => "#"
      t.timestamps
    end
  end
end
