class CreateSpreeSpreeSliderThemeThemeColors < ActiveRecord::Migration
  def change
    create_table :spree_spree_slider_theme_theme_colors do |t|
      t.string :color
      t.string :filename
      t.string :used, :default => "false"

      t.timestamps
    end
  end
end
