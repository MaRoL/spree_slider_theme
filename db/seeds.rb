if Spree::SpreeSliderTheme::Info.count == 0
  if defined? Spree::GlabsPaymentMethod
    info = Spree::SpreeSliderTheme::Info.new
    info.title = "confirm_transfer"
    info.save
  end

  info = Spree::SpreeSliderTheme::Info.new
  info.title = "contact_us"
  info.save

  info = Spree::SpreeSliderTheme::Info.new
  info.title = "how_to_shop"
  info.save
end

if Spree::SpreeSliderTheme::ThemeColor.count == 0
  theme_color = Spree::SpreeSliderTheme::ThemeColor.new
  theme_color.color = "default(red)"
  theme_color.filename = "default"
  theme_color.used = "true"
  theme_color.save
end

if Spree::SpreeSliderTheme::ThemeImage.count == 0
  @no_image = File.open("public/spree_slider_theme/no_image.jpg")
  theme_image = Spree::SpreeSliderTheme::ThemeImage.create(:image => @no_image)
  theme_image.enabled = "true"
  theme_image.save
end
