SpreeSliderTheme
================

SpreeSliderTheme is a theme for Garasilabs.

Add spree_slider_theme to Gemfile
    $ bundle
    $ rails g spree_slider_theme:install

If there's some errors aim to required file, you can remove this errors
with remove lines in app/assets/javascripts/ and app/assets/stylesheets/

Copyright (c) 2012 PT Garasilabs Manivesta, released under the New BSD License
